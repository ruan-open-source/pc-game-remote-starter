# PC Game Remote Starter

An app that scans a PC for games and lists the games with the option to start a game when a keyboard and mouse are unavailable such as when playing games on the couch with a controller.